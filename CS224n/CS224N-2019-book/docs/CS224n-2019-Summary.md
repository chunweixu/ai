# CS224n-2019-Summary

-   本文将结合课程提供的材料、作者自己记录的中文笔记以及 Reference 中他人笔记，对课程的重点知识进行梳理与总结

## Summary

### Lecture 01 Introduction and Word Vectors

本节课介绍了NLP研究的

-   人类语言具有信息功能与社会功能
-   传统的 WordNet 以及 one-hot 方法在词义表达任务中存在的问题
-   Word2vec 框架的思想与具体思路
-   

## Reference

以下是学习本课程时的可用参考书籍：

[《基于深度学习的自然语言处理》](<https://item.jd.com/12355569.html>) 

[《神经网络与深度学习》](<https://nndl.github.io/>)

以下是整理笔记的过程中参考的博客：

[斯坦福CS224N深度学习自然语言处理2019冬学习笔记目录](<https://zhuanlan.zhihu.com/p/59011576>) (课件核心内容的提炼，并包含作者的见解与建议)

[斯坦福大学 CS224n自然语言处理与深度学习笔记汇总](<https://zhuanlan.zhihu.com/p/31977759>) {>>这是针对note部分的翻译<<}